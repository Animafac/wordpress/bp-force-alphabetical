<?php
/**
 * Plugin Name: BuddyPress Force Alphabetical Order
 * Description: Always display BuddyPress members in alphabetical order.
 * Plugin URI: https://framagit.org/Animafac/bp-force-alphabetical
 * Author: Animafac
 * Author URI: https://www.animafac.net/
 * Version: 0.1.0
 */

use BpForceAlphabetical\BpForceAlphabetical;

require_once __DIR__.'/vendor/autoload.php';

add_filter(
    'bp_ajax_querystring',
    [BpForceAlphabetical::class, 'forceQueryString'],
    999
);

add_action(
    'wp_enqueue_scripts',
    [BpForceAlphabetical::class, 'addCustomCss'],
    999
);
