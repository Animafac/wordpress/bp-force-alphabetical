<?php
/**
 * BpForceAlphabetical class.
 */

namespace BpForceAlphabetical;

/**
 * Main plugin class.
 */
class BpForceAlphabetical
{

    /**
     * Force the type parameter in the BuddyPress query string.
     * @param  string $query BuddyPress query string
     * @return string
     */
    public static function forceQueryString($query)
    {
        return $query.'&type=alphabetical';
    }

    /**
     * Add custom CSS to BuddyPress CSS.
     */
    public static function addCustomCss()
    {
        wp_add_inline_style(
            'bp-legacy-css',
            '#members-order-select { display: none; }'
        );
    }
}
