# BuddyPress Force Alphabetical Order

By default BuddyPress sorts members by latest activity and in this mode,
it only displays members which have logged in at least once.

On some websites this does not make sense and you need to display every member every time.
So this plugin forces the member list to display in alphabetical order and to display members even if they don't have any activity yet.
